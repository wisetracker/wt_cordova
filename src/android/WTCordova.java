
package kr.co.wisetracker.cordova


import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by caspar on 15. 7. 16..
 */
public class WTCordova extends CordovaPlugin{
    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {

        @Override
        public void initialize(CordovaInterface cordova, CordovaWebView webView) {
            super.initialize(cordova, webView);
            // your init code here
        }
        if (action.equals("greet")) {

            String name = data.getString(0);
            String message = "Hello, " + name;
            callbackContext.success(message);

            return true;

        } else {

            return false;

        }
    }
}
